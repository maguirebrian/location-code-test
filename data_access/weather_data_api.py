import requests

WEATHER_API_URL = "https://www.metaweather.com"

class WeatherData():

    def __init__(self):
        # Using a temporary variable in place of an in memory db such as Redis, which would be more suitable for production
        self.woeid_cache = {}

    def get_weather_data_by_city_name(self, city_name, date):
        forecast = None
        woeid = self.get_woeid_by_city_name(city_name)
        if woeid:
            forecast = self.get_weather_data_by_woeid(woeid, date)
        return forecast

    def _update_woeid_cache(self, city_name, woeid):
        self.woeid_cache['city_name'] = woeid

    def _check_woeid_cache(self, city_name):
        return self.woeid_cache.get(city_name)

    def get_woeid_by_city_name(self, city_name):
        woeid = self._check_woeid_cache(city_name)
        if not woeid:
            r = requests.get("{0}/api/location/search".format(WEATHER_API_URL), params={'query': city_name})

            if r.status_code == 200 and r.json():
                # api returns a list of city names, some may match part of the city name given, so must find complete match
                for city_info in r.json():
                    if city_info.get('title').lower() == city_name.lower():
                        woeid = city_info.get('woeid')
                        self._update_woeid_cache(city_name, woeid)
        return woeid

    @staticmethod
    def get_weather_data_by_woeid(woeid, date):
        requested_forcast = None
        r = requests.get("{0}/api/location/{1}".format(WEATHER_API_URL, woeid))
        if r.status_code == 200:
            for forcast in r.json().get('consolidated_weather', []):
                # This needs to take into account different time zones
                # TODO take into account different time zones
                if forcast.get('applicable_date') == date:
                    requested_forcast = forcast
        return requested_forcast
