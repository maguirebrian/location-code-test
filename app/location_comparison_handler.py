from tornado.web import RequestHandler
from http import HTTPStatus
import json
import operator

import tornado.web
from tornado.concurrent import run_on_executor

from city import City


class LocationComparisonHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        self.executor = application.executor
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def initialize_city(self, city_name, data_handler):
        return City.initialize_city(city_name, data_handler)

    @tornado.gen.coroutine
    def get(self, *args, **kwargs):
        """
        Retrieves information about multiple cities, rates them and returns a ranking and score for each city.
        :param args:
        :param kwargs:
        :return:
        """
        city_name_list = args[0].split(',')
        city_list = []
        for name in city_name_list:
            new_city = self.initialize_city(name, self.application.data_handlers)
            if new_city:
                city_list.append({'city_name': name, 'city_score': new_city.calc_score()})
        sorted_city_list = sorted(city_list, key=operator.itemgetter('city_score'), reverse=True)
        for i in range(0, len(sorted_city_list)):
            sorted_city_list[i].update({'city_rank': i+1})
        response = {'city_data': sorted_city_list}

        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
