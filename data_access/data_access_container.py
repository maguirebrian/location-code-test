from geographic_data import GeographicData
from weather_data_api import WeatherData


class DataAccessContainer:

    def __init__(self):
        self.geographic_data = GeographicData()
        self.weather_data = WeatherData()