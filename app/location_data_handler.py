import json
from http import HTTPStatus

import tornado.web
from tornado.web import RequestHandler
from tornado.concurrent import run_on_executor

from app.city import City


class LocationHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        self.executor = application.executor
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    @run_on_executor()
    def initialize_city(self, city_name, data_handler):
        return City.initialize_city(city_name, data_handler)

    @tornado.gen.coroutine
    def get(self, *args, **kwargs):
        """
        Retrieve information about a city.
        :param args:
        :param kwargs:
        :return:
        """
        city_name = args[0]
        city = yield self.initialize_city(city_name, self.application.data_handlers)
        if not city:
            self.set_status(HTTPStatus.BAD_REQUEST)
            self.write_error(HTTPStatus.BAD_REQUEST)
        else:
            response = city.get_city_info_dict()
            self.set_status(HTTPStatus.OK)
            self.write(json.dumps(response))
