import csv


class GeographicData:

    CSV_FILE_LOCATION = "data/european_cities.csv"

    def __init__(self):
        self.city_index_cache = {}

    def get_city_info_from_cache(self, city_name):
        return self.city_index_cache.get(city_name)

    def set_city_info_in_cache(self, city_name, city_info):
        self.city_index_cache[city_name] = city_info

    def get_city_info_by_name(self, city_name):
        city_info = self.get_city_info_from_cache(city_name)
        if not city_info:   # add check for cache timeout
            with open(self.CSV_FILE_LOCATION) as csv_file:
                reader = csv.DictReader(csv_file)
                for row in reader:
                    if row['city'].lower() == city_name.lower():
                        city_info = row
                        self.set_city_info_in_cache(city_name, row)
                        break
        return city_info
