import datetime
import unittest

from data_access import *


class TestWeatherDataMethods(unittest.TestCase):

    def setUp(self):
        self.weather_data = WeatherData()

    def test_get_woeid_by_city_name(self):
        self.assertEqual(self.weather_data.get_woeid_by_city_name('Dublin'), 560743)

    def test_get_woeid_by_city_name_case_sensitive(self):
        self.assertEqual(self.weather_data.get_woeid_by_city_name('duBliN'), 560743)

    def test_get_woeid_by_city_name_case_invalid_name(self):
        self.assertIsNone(self.weather_data.get_woeid_by_city_name(''))

    def test_get_weather_data_by_city_name_correct_date(self):
        date = datetime.date.today().strftime("%Y-%m-%d")
        self.assertEqual(self.weather_data.get_weather_data_by_city_name('Amsterdam', date).get('applicable_date'), date)


if __name__ == '__main__':
    unittest.main()