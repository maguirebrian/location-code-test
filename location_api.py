import tornado.ioloop

from concurrent.futures import ThreadPoolExecutor

from app.location_comparison_handler import LocationComparisonHandler
from app.location_data_handler import LocationHandler
from data_access.data_access_container import DataAccessContainer

MAX_WORKERS = 5


# Adding an extended class of the tornado application class. This allows for use of a simple cache for geographical data.
# In production, the use of a shared cache db, redis for example would remove the need for this.
class TornadoAppWithCache(tornado.web.Application):
    def __init__(self, paths, **kwargs):
        self.data_handlers = DataAccessContainer()
        self.executor = ThreadPoolExecutor(max_workers=MAX_WORKERS)
        super().__init__(paths, **kwargs)


def make_app():
    # return tornado.web.Application([(r"/location-data/([a-zA-Z0-9]*)?", LocationHandler, {}),
    #                                 (r"/location-comparison/cities=\[(\S+)\]", LocationComparisonHandler, {})])
    return TornadoAppWithCache([(r"/location-data/([a-zA-Z0-9]*)?", LocationHandler, {}),
                                (r"/location-comparison/cities=\[(\S+)\]", LocationComparisonHandler, {})])


if __name__ == "__main__":
    application = make_app()
    application.listen(8484)
    tornado.ioloop.IOLoop.current().start()
