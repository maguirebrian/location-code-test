import datetime
from random import randint

COLD = 15
WARM = 20
HOT = 25
VERY_HOT = 30
TOO_HOT = 35
MAX_BARS = 12733

class City:

    def __init__(self, name, data_access_container):
        self.name = name.lower()
        self.data_access_container = data_access_container
        self.cur_temp = None
        self.cur_temp_des = None
        self.population = None
        self.num_bars = None
        self.public_transport_rating = None

    def set_weather(self):
        weather_data = self.data_access_container.weather_data.get_weather_data_by_city_name(
            self.name, datetime.date.today().strftime("%Y-%m-%d"))
        if not weather_data:
            return False
        self.cur_temp = weather_data.get('the_temp')
        self.cur_temp_des = weather_data.get('weather_state_name')
        return True

    @staticmethod
    def initialize_city(city_name, data_handler):
        new_city = City(city_name, data_handler)
        if new_city.set_city_info() and new_city.set_weather():
            return new_city
        else:
            return None

    def set_city_info(self):
        city_data = self.data_access_container.geographic_data.get_city_info_by_name(self.name)
        if not city_data:
            return False
        self.population = int(city_data.get('population'))
        self.num_bars = int(city_data.get('bars'))
        self.public_transport_rating = float(city_data.get('public_transport'))
        return True

    @staticmethod
    def calc_temp_score(temp):
        if temp < COLD:
            temp_score = 2
        elif COLD < temp < WARM:
            temp_score = 4
        elif WARM < temp < HOT:
            temp_score = 10
        elif HOT < temp < TOO_HOT:
            temp_score = 8
        else:
            temp_score = 0
        return temp_score

    # bar score is given as a percentage of the maximum bars that a city has in the database.
    @staticmethod
    def calc_bar_score(num_bars):
        bar_score = (num_bars/MAX_BARS)*10
        if bar_score > 10:
            bar_score = 10
        return bar_score

    def calc_score(self):
        return (self.calc_bar_score(self.num_bars) + self.calc_temp_score(self.cur_temp))/2

    def get_city_info_dict(self):
        return {'city_name': self.name,
                'current_temperature': self.cur_temp,
                'current_weather_description': self.cur_temp_des,
                'population': self.population,
                'bars': self.num_bars,
                'city_score': self.calc_score()}
